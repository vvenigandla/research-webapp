import React, { Component } from 'react';
import './App.css';

class App extends Component {

  

  constructor(props){
    super(props);
    this.state = {
      ipItem:{
        transactionId:React.createRef(),
        agency:React.createRef(),
        Carrier:React.createRef(),
        Date:React.createRef(),
        Amount:React.createRef()
      },
      file:React.createRef(),
      items:[]
    };

    this.insertRecord = this.insertRecord.bind(this);
    this.uploadFile = this.uploadFile.bind(this);
  }

  componentDidMount(){
    fetch("https://29thks5sh3.execute-api.us-east-1.amazonaws.com/default/getResearchTransaction", {
        method: 'POST',
        headers: new Headers({
            "Content-Type": "application/json",
            "Accept":"application/json"
        }),
        mode: "cors"
    })
    .then(response => response.json())
    .then(response => {
        console.log(response);
        this.setState({items:response.Items});
    });
  }

  insertRecord(event){
    event.preventDefault();

    const req = {
      Item: {
        transactionId: this.state.ipItem.transactionId.current.value,
        agency: this.state.ipItem.agency.current.value,
        Carrier: this.state.ipItem.Carrier.current.value,
        Date: this.state.ipItem.Date.current.value,
        Amount: this.state.ipItem.Amount.current.value
      }
    };

    fetch("https://4sk9pjt6i1.execute-api.us-east-1.amazonaws.com/default/addResearchTransaction", {
        method: 'POST',
        headers: {
            'Accept':'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        mode: "cors",
        body: JSON.stringify(req)
    })
    .then(response => {
            console.log(response);
            this.setState({ items: [...this.state.items, req.Item] })
    });
  }

  uploadFile(event){console.log(this.state.file.current.files[0].name);
    event.preventDefault();

    let formData  = new FormData();
    //formData.append("fName", this.state.file.current.files[0].name);
    formData.append("file", this.state.file.current.files[0]); 

    this.handleFileSelect(this.state.file.current.files[0]);

    
  }

  upload(f) {
    fetch("https://6d2j95pbo7.execute-api.us-east-1.amazonaws.com/default/uploadResearchTransaction", {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        mode: "cors",
        body: JSON.stringify({
          "fName": this.state.file.current.files[0].name,
          "upFile": f
        })
    })
    .then(response => {
            console.log(response);
    });
  }

  handleFileSelect(f) {
    const PT = this;
    let reader = new FileReader();
    reader.onload = (function(theFile) {
      return function(e) {
        /*let binaryData = e.target.result;
        let base64String = window.btoa(binaryData);*/

        PT.upload(reader['result']);
      };
    })(f);
    reader.readAsDataURL(f);
  }

  render() {
    return (
      <div>
        <header className="row">
          <h1>Research - REACTJS - POC</h1>
        </header>
        <section className="body row">
          <div className="row">
            <form>
            <table className="customGrid">
              <thead>
                <tr>
                  <th>Transaction #</th>
                  <th>Agency</th>
                  <th>Carrier</th>
                  <th>Date</th>
                  <th>Amount</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><input type="text" name="transactionId" ref={this.state.ipItem.transactionId} /></td>
                  <td><input type="text" name="agency" ref={this.state.ipItem.agency} /></td>
                  <td><input type="text" name="Carrier" ref={this.state.ipItem.Carrier} /></td>
                  <td><input type="text" name="Date" ref={this.state.ipItem.Date} /></td>
                  <td><input type="text" name="Amount" ref={this.state.ipItem.Amount} /></td>
                  <td><button onClick={this.insertRecord}>Add</button></td>
                </tr>
                <tr>
                  <td><input type="file" name="file" ref={this.state.file} /></td>
                  <td><button onClick={this.uploadFile}>Upload File to S3</button></td>
                </tr>
              </tbody>
            </table>
            </form>
          </div>
          <div className="row table">
            <table className="customGrid">
              <thead>
                <tr>
                  <th>Transaction #</th>
                  <th>Agency</th>
                  <th>Carrier</th>
                  <th>Date</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                {this.state.items.map(el => 
                  <tr>
                    <td>{el.transactionId}</td>
                    <td>{el.agency}</td>
                    <td>{el.Carrier}</td>
                    <td>{el.Date}</td>
                    <td>{el.Amount}</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </section>
        <footer className="row">
          <p>&copy; 2019 ARC - Airlines Reporting Corporation</p>
          <p>All rights reserved</p>
        </footer>
      </div>
    );
  }
}

export default App;
